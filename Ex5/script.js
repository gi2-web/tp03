let somme = function(T) {
    let sum = 0
    for(let i = 0; i < T.length; i++) {
        sum += T[i]
    }
    return sum
}

let fsomme =  function(a, b, c, d) {
    return somme(arguments)
}

let dif_max = function(T) {
    let max = T[1] - T[0]
    for(let i = 1; i < T.length - 1; i++) {
        dif = T[i+1] - T[i]
        if(dif > max)
            max = T[i+1] - T[i]
    }
    return max
}

let T = [1, 3, 8, 10, 5]
alert(`sum est : ${somme(T)}`)
alert(`sum des arguments est : ${fsomme(0, 3, 7, 9)}`)
alert(`la différence maximale est : ${dif_max(T)}`)