let isDiagonal = function(M) {
    let isDiagonal = true
    for (let i = 0; i < M.length; i++) {
        if(M[i][i] !== 0){
            isDiagonal = false
            break
        }
    }
    return isDiagonal
}

let M = [[0,2,3], [4,0,6], [7,8,0]]
alert(`la matrice diagonale : ${isDiagonal(M)}`)